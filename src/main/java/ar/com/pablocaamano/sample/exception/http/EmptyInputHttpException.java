package ar.com.pablocaamano.sample.exception.http;

import ar.com.pablocaamano.commons.exception.http.ICommonHttpException;
import org.springframework.http.HttpStatus;

public class EmptyInputHttpException extends RuntimeException implements ICommonHttpException {

    private HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
    private String code = null;
    private String description = null;

    public EmptyInputHttpException(Throwable cause) {
        super(cause);
        this.description = cause.getMessage();
    }


    public EmptyInputHttpException(Throwable cause, String code) {
        super(cause);
        this.description = cause.getMessage();
        this.code = code;
    }


    @Override
    public HttpStatus getStatus() {
        return httpStatus;
    }

    public String getDescription(){
        return this.description;
    }

    public String getCode(){
        return this.code;
    }
}
