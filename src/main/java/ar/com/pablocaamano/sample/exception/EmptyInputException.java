package ar.com.pablocaamano.sample.exception;

import ar.com.pablocaamano.commons.exception.CommonException;

public class EmptyInputException extends CommonException {

    private static final String DEFAULT_MESSAGE = "Invalid param received, the object is null or empty";

    public EmptyInputException() {
        super(DEFAULT_MESSAGE);
    }

    public EmptyInputException(Throwable cause) {
        super(DEFAULT_MESSAGE, cause);
    }

}
