package ar.com.pablocaamano.sample.controller;

import ar.com.pablocaamano.sample.configuration.ApiConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;

@RestController
public class MainController {

    private static final Logger logger = LoggerFactory.getLogger(MainController.class);
    private static final String endpointBasePath = "/actuator";

    @Autowired
    private ApiConfiguration apiConfiguration;

    @RequestMapping(value = "/")
    public ResponseEntity<String> home(HttpServletRequest request){
        logger.info("[SAMPLE-API] - Actuator endpoint");
        String contextPath = request.getContextPath();
        String url = "http://" + apiConfiguration.getApiHost() + ":" + apiConfiguration.getManagementPort()  + contextPath;

        String response = "<h2>" + apiConfiguration.getApplicationName() + "</h2>" +
                "<ul><li><a href='" + url + endpointBasePath + "/health'>Actuator health status</a></li>" +
                "<li><a href='" + url + endpointBasePath + "'>All actuator endpoints</a></li></ul>";
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
