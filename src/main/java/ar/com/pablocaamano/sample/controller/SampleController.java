package ar.com.pablocaamano.sample.controller;

import ar.com.pablocaamano.commons.builder.ResponseBuilder;
import ar.com.pablocaamano.commons.rest.RestResponse;
import ar.com.pablocaamano.sample.exception.EmptyInputException;
import ar.com.pablocaamano.sample.service.SampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.Map;

@RestController
@RequestMapping(value = "/sample")
public class SampleController {

    private static final Logger logger = LoggerFactory.getLogger(SampleController.class);

    @Autowired
    private SampleService sampleService;

    @GetMapping(value = "/test")
    public ResponseEntity<RestResponse> sampleGetEndpoint(
            HttpServletRequest request
    ){
        logger.info("[SAMPLE-API] - " + request.getMethod() + "_" + request.getRequestURI() + " endpoint called");
        return new ResponseEntity<>(
                ResponseBuilder.init()
                        .addRequestCode("200")
                        .addRequestMethod(request.getMethod())
                        .addRequestUri(request.getRequestURI())
                        .addTimestamp(new Timestamp(System.currentTimeMillis()).toString())
                        .addData("Get request Ok")
                        .build(),
                HttpStatus.OK
        );
    }

    @PostMapping(value = "/test")
    public ResponseEntity<RestResponse> samplePostEndpoint(
            @RequestBody Map<String, String> requestBody,
            HttpServletRequest request
    ){
        try{
            logger.info("[SAMPLE-API] - " + request.getMethod() + "_" + request.getRequestURI() + " endpoint called");
            sampleService.testExceptions(requestBody);
            logger.info("[SAMPLE-API] - generating response");
            return new ResponseEntity<>(
                    ResponseBuilder.init()
                            .addRequestMethod(request.getMethod())
                            .addRequestUri(request.getRequestURI())
                            .addTimestamp("")
                            .addData("Get request Ok")
                            .build(),
                    HttpStatus.OK
            );
        }catch (EmptyInputException exception){
            throw new EmptyInputException(exception);
        }
    }

}
