package ar.com.pablocaamano.sample.service;

import java.util.Map;

public interface SampleService {
    Map<String, String> testExceptions(Map<String, String> param);
}
