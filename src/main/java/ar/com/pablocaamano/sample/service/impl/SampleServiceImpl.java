package ar.com.pablocaamano.sample.service.impl;

import ar.com.pablocaamano.sample.exception.EmptyInputException;
import ar.com.pablocaamano.sample.service.SampleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SampleServiceImpl implements SampleService {

    private static final Logger logger = LoggerFactory.getLogger(SampleServiceImpl.class);

    @Override
    public Map<String, String> testExceptions(Map<String, String> param) {
        if (param != null){
            logger.info("[SAMPLE-API] - Service is processing request");
            param.put("STATUS", "processed");
            return param;
        }
        throw new EmptyInputException();
    }
}
